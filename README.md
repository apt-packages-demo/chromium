# [[]](https://apt-packages-demo.gitlab.io/chromium/) chromium

Web browser
* https://tracker.debian.org/chromium
* https://launchpad.net/ubuntu/+source/chromium-browser

![](https://apt-packages-demo.gitlab.io/chromium/screenshot.png)

# chromedriver
* https://chromedriver.storage.googleapis.com/LATEST_RELEASE

# Official documentation
* [*Getting Started with Headless Chrome*
  ](https://developers.google.com/web/updates/2017/04/headless-chrome)
  2019-05 Eric Bidelman

# Ubuntu support for chromium-browser and snap(d)

```sh
sudo add-apt-repository ppa:chromium-team/stable
sudo apt-get update
sudo apt install chromium
```
* [*How to remove snap completely without losing the Chromium browser?*
  ](https://askubuntu.com/questions/1179273/how-to-remove-snap-completely-without-losing-the-chromium-browser)
```sh
sudo add-apt-repository ppa:chromium-team/dev
sudo apt-get update
```
* [Chromium team](https://launchpad.net/~chromium-team/+archive/ubuntu/dev)

* [*How to Install Google Chrome in Ubuntu & Linux Mint*
  ](https://www.omgubuntu.co.uk/how-to-install-google-chrome-on-ubuntu)
  (2021-07) Joey Sneddon
* [*How to install Google Chrome web browser on Ubuntu 19.10 Eoan Ermine Linux*
  ](https://linuxconfig.org/how-to-install-google-chrome-web-browser-on-ubuntu-19-10-eoan-ermine-linux)
  2019-10 LinuxConfig.org
